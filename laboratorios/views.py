from django.shortcuts import render
from .models import Laboratorio
from .models import Producto
from .models import Tipoproducto
from .models import Consejo
from .serializers import LaboratorioSerializer
from .serializers import ProductoSerializer
from .serializers import ConsejoSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

class LaboratorioList(APIView):
    def get(self, request, format=None):
        laboratorios = Laboratorio.objects.all()
        serializer = LaboratorioSerializer(laboratorios, many=True)
        return Response(serializer.data)

class LaboratorioSimple(APIView):
    def get(self, request, pk, format=None):
        laboratorio = Laboratorio.objects.get(pk=pk)
        serializer = LaboratorioSerializer(laboratorio)
        return Response(serializer.data)

class ProductoList(APIView):
    def get(self, request, pk, format=None):
        productos = Producto.objects.filter(laboratorio=pk)
        serializer = ProductoSerializer(productos, many=True)
        return Response(serializer.data)

class ProductoSimple(APIView):
    def get(self, request, pk, format=None):
        producto = Producto.objects.get(pk=pk)
        serializer = ProductoSerializer(producto)
        return Response(serializer.data)

class ProductoBusqueda(APIView):
    def get(self, request, busqueda, format=None):
        #busquedas
        productosnombre = Producto.objects.filter(nombre__icontains=busqueda)
        productosusos = Producto.objects.filter(usos__icontains=busqueda)
        productosdescripcion = Producto.objects.filter(descripcion__icontains=busqueda)

        #union de las busquedas
        productos = productosnombre.all() | productosusos.all() | productosdescripcion.all()

        #se asegura que no haya productos repetidos en el query
        productos = productos.distinct()

        serializer = ProductoSerializer(productos, many=True)
        return Response(serializer.data)

class ProductoTipo(APIView):
    def get(self, request, tipo, format=None):
        tipoproducto = Tipoproducto.objects.get(tipo=tipo)
        productos = Producto.objects.filter(tipoproducto=tipoproducto)
        serializer = ProductoSerializer(productos, many=True)
        return Response(serializer.data)

class ConsejoSimple(APIView):
    def get(self, request, format=None):
        consejo = Consejo.objects.first()
        serializer = ConsejoSerializer(consejo)
        return Response(serializer.data)
