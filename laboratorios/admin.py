from django.contrib import admin
from .models import Tipoproducto, Producto, Laboratorio, Consejo, Tienda

class TipoproductoAdmin(admin.ModelAdmin):
    model = Tipoproducto

class TiendaAdmin(admin.ModelAdmin):
    model = Tienda
    extra = 1

class ProductoInline(admin.StackedInline):
    model = Producto
    extra = 1

class ProductoAdmin(admin.ModelAdmin):
    model = Producto
    extra = 1

class LaboratorioAdmin(admin.ModelAdmin):
    fieldsets = [
            (None, {'fields': ['nombre', 'descripcion', 'imagen']}),
    ]
    inlines = [ProductoInline]

class ConsejoAdmin(admin.ModelAdmin):
    model = Consejo
    extra = 1



admin.site.register(Laboratorio, LaboratorioAdmin)
admin.site.register(Producto, ProductoAdmin)
admin.site.register(Tipoproducto, TipoproductoAdmin)
admin.site.register(Consejo, ConsejoAdmin)
admin.site.register(Tienda, TiendaAdmin)

# Register your models here.
