from rest_framework import serializers
from .models import Laboratorio
from .models import Producto
from .models import Consejo
from .models import Tienda 

class TiendaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tienda
        fields = ('id', 'nombre', 'telefono', 'direccion')


class LaboratorioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Laboratorio
        fields = ('id', 'nombre', 'descripcion', 'imagen')

class ProductoSerializer(serializers.ModelSerializer):
    tiendas = TiendaSerializer(many=True, read_only=True)
    laboratorio = serializers.StringRelatedField()
    class Meta:
        model = Producto
        fields = ('laboratorio', 'tiendas', 'id', 'tipoproducto',  'nombre', 'usos', 'descripcion', 'composicion', 'dosificacion','presentacion', 'numeroDeRegistro', 'imagen', 'contraindicaciones')

class ConsejoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Consejo
        fields = ('producto', 'id', 'texto')
