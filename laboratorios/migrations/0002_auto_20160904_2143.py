# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('laboratorios', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='laboratorio',
            name='imagen',
            field=models.ImageField(default='static/uploads/laboratorios/lab.png', upload_to='static/uploads/laboratorios'),
        ),
        migrations.AddField(
            model_name='producto',
            name='imagen',
            field=models.ImageField(default='static/uploads/productos/product.png', upload_to='static/uploads/productos'),
        ),
    ]
