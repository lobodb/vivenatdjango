# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('laboratorios', '0007_auto_20160930_2050'),
    ]

    operations = [
        migrations.AlterField(
            model_name='producto',
            name='composicion',
            field=models.TextField(max_length=500),
        ),
        migrations.AlterField(
            model_name='producto',
            name='contraindicaciones',
            field=models.TextField(max_length=500, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='producto',
            name='descripcion',
            field=models.TextField(max_length=500, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='producto',
            name='dosificacion',
            field=models.TextField(max_length=500),
        ),
        migrations.AlterField(
            model_name='producto',
            name='presentacion',
            field=models.TextField(max_length=500),
        ),
        migrations.AlterField(
            model_name='producto',
            name='usos',
            field=models.TextField(max_length=500),
        ),
    ]
