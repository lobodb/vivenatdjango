# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('laboratorios', '0002_auto_20160904_2143'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tipoproducto',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, serialize=False, verbose_name='ID')),
                ('tipo', models.CharField(max_length=50)),
            ],
        ),
        migrations.AlterField(
            model_name='laboratorio',
            name='imagen',
            field=models.ImageField(default='laboratorios/lab.png', upload_to='laboratorios'),
        ),
        migrations.AlterField(
            model_name='producto',
            name='imagen',
            field=models.ImageField(default='productos/product.png', upload_to='productos'),
        ),
        migrations.AddField(
            model_name='producto',
            name='tipoproducto',
            field=models.ForeignKey(default=1, to='laboratorios.Tipoproducto'),
            preserve_default=False,
        ),
    ]
