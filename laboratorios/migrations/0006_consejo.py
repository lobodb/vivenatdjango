# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('laboratorios', '0005_producto_descripcion'),
    ]

    operations = [
        migrations.CreateModel(
            name='Consejo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('texto', models.CharField(max_length=300)),
                ('producto', models.ForeignKey(to='laboratorios.Producto')),
            ],
        ),
    ]
