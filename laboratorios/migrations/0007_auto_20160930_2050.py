# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('laboratorios', '0006_consejo'),
    ]

    operations = [
        migrations.AddField(
            model_name='producto',
            name='contraindicaciones',
            field=models.TextField(max_length=200, default=''),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='consejo',
            name='texto',
            field=models.TextField(max_length=300),
        ),
        migrations.AlterField(
            model_name='laboratorio',
            name='descripcion',
            field=models.TextField(max_length=500),
        ),
        migrations.AlterField(
            model_name='producto',
            name='descripcion',
            field=models.TextField(max_length=500),
        ),
    ]
