# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('laboratorios', '0003_auto_20160918_2320'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='tipoproducto',
            options={'verbose_name': 'Tipo de producto', 'verbose_name_plural': 'Tipos de producto'},
        ),
        migrations.RenameField(
            model_name='producto',
            old_name='descripcion',
            new_name='usos',
        ),
        migrations.AddField(
            model_name='producto',
            name='composicion',
            field=models.CharField(max_length=200, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='producto',
            name='dosificacion',
            field=models.CharField(max_length=200, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='producto',
            name='presentacion',
            field=models.CharField(max_length=200, default=''),
            preserve_default=False,
        ),
    ]
