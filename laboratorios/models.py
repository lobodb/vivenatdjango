from django.db import models

class Tienda(models.Model):
    nombre = models.CharField(max_length=200)
    telefono = models.CharField(max_length=20)
    direccion = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre


class Tipoproducto(models.Model):
    tipo = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'Tipo de producto'
        verbose_name_plural = 'Tipos de producto'

    def __str__(self):
        return self.tipo

class Laboratorio(models.Model):
    nombre = models.CharField(max_length=200)
    descripcion = models.TextField(max_length=500)
    imagen = models.ImageField(upload_to = "laboratorios", default="laboratorios/lab.png")

    def __str__(self):
        return self.nombre

class Producto(models.Model):
    laboratorio = models.ForeignKey(Laboratorio)
    tipoproducto = models.ForeignKey(Tipoproducto)
    tiendas = models.ManyToManyField(Tienda)

    nombre = models.CharField(max_length=200)
    descripcion = models.TextField(max_length=500,null=True, blank=True)
    usos = models.TextField(max_length=500)
    composicion = models.TextField(max_length=500)
    dosificacion = models.TextField(max_length=500)
    presentacion = models.TextField(max_length=500)
    numeroDeRegistro = models.CharField(max_length=30)
    contraindicaciones = models.TextField(max_length=500, null=True, blank=True)

    imagen = models.ImageField(upload_to = "productos", default="productos/product.png")

    def __str__(self):
        return self.nombre

class Consejo(models.Model):
    producto = models.ForeignKey(Producto)
    texto = models.TextField(max_length=300)

    def __str__(self):
        return str(self.id)
