from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from laboratorios import views

urlpatterns = [
    #lista todos los laboratorios
    url(r'^laboratorios/$', views.LaboratorioList.as_view()),

    #lista un laboratorio en especifico
    url(r'^laboratorio/(?P<pk>[0-9]+)$', views.LaboratorioSimple.as_view()),

    #lista los productos que pertenecen a un laboratorio
    url(r'^productos/(?P<pk>[0-9]+)$', views.ProductoList.as_view()),

    #devuelve una lista de productos segun el tipo
    url(r'^productos/tipo/(?P<tipo>[\w]+)/$', views.ProductoTipo.as_view()),

    #lista un producto en especifico
    url(r'^producto/(?P<pk>[0-9]+)$', views.ProductoSimple.as_view()),

    #devuelve una lista de productos segun la busqueda
    url(r'^productos/busqueda/(?P<busqueda>[\w ]+)/$', views.ProductoBusqueda.as_view()),

    url(r'^consejo/$', views.ConsejoSimple.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
