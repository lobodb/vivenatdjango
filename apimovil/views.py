from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import UsuarioMovil
from .serializers import UsuarioSerializer

@api_view(['POST'])
def usuariomovil_create(request):
    serializer = UsuarioSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'PUT', 'DELETE'])
def usuariomovil_detail(request, pk, format=None):
    try:
        usuariomovil = UsuarioMovil.objects.get(pk=pk)
    except UsuarioMovil.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = UsuarioSerializer(usuariomovil)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = UsuarioSerializer(usuariomovil, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'DELETE':
        usuariomovil.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
