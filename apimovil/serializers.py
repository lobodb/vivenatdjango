from rest_framework import serializers
from .models import UsuarioMovil

class UsuarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = UsuarioMovil
        fields = ('id', 'nombre', 'password')

