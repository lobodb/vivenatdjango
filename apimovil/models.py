from django.db import models

class UsuarioMovil(models.Model):
    nombre = models.CharField(max_length=20)
    password = models.CharField(max_length=30)

    class Meta:
        verbose_name = "Usuario móvil"
        verbose_name_plural = "Usuarios móviles"

    def __str__(self):
        return self.nombre
