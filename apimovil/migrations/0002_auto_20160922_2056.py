# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apimovil', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='usuariomovil',
            options={'verbose_name': 'Usuario móvil', 'verbose_name_plural': 'Usuarios móviles'},
        ),
    ]
