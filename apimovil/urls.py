from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from apimovil import views

urlpatterns = [
    url(r'^usuarios/$', views.usuariomovil_create),
    url(r'^usuarios/(?P<pk>[0-9]+)$', views.usuariomovil_detail),
]
urlpatterns = format_suffix_patterns(urlpatterns)
