from django.contrib import admin
from .models import Tipocliente, Cliente

class TipoclienteAdmin(admin.ModelAdmin):
    model = Tipocliente

class ClienteAdmin(admin.ModelAdmin):
    model = Cliente

admin.site.register(Tipocliente, TipoclienteAdmin)
admin.site.register(Cliente, ClienteAdmin)
