from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import RegistroClienteForm

# Create your views here.
def registroCliente(request):
    if request.method == "POST":
        return HttpResponseRedirect('/gracias')
    else:
        form = RegistroClienteForm(request.POST or None)
        context = { 
                "form": form 
        }

        if form.is_valid():
            instance = form.save(commit=False)
            instance.save()

        return render(request, "registro.html", context)
