from django import forms
from django.core.exceptions import NON_FIELD_ERRORS

from .models import Tipocliente, Cliente

class RegistroClienteForm(forms.ModelForm):
    class Meta:
        model = Cliente
        fields = [ 'tipo','empresa','nit', 'ciudad', 'contacto','telefono','email']
        labels = {
                'tipo': ('tipo de empresa'),
                'empresa': ('nombre de la empresa'),
                'contacto': ('nombre del contacto'),
        }
