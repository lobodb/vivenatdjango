from django.conf.urls import url
from django.views.generic import TemplateView
from portalweb import views
app_name = 'portalweb'
urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='index.html')),
    url(r'^registro/$', views.registroCliente),
    url(r'^gracias/$', TemplateView.as_view(template_name='gracias.html'), name='gracias'),
]
