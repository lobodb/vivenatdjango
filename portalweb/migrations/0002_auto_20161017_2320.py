# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portalweb', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='cliente',
            name='ciudad',
            field=models.CharField(max_length=30, default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='cliente',
            name='nit',
            field=models.CharField(max_length=30, default='', error_messages={'unique': 'Ya existe una empresa registrada con este NIT'}, unique=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='cliente',
            name='empresa',
            field=models.CharField(max_length=30, error_messages={'unique': 'Ya existe una empresa registrada con este nombre'}, unique=True),
        ),
    ]
