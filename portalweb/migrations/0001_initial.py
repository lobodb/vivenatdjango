# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Cliente',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('empresa', models.CharField(max_length=30, unique=True)),
                ('contacto', models.CharField(max_length=50)),
                ('telefono', models.CharField(max_length=30)),
                ('email', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Tipocliente',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('tipo', models.CharField(max_length=30)),
            ],
            options={
                'verbose_name': 'Tipo de cliente',
                'verbose_name_plural': 'Tipos de cliente',
            },
        ),
        migrations.AddField(
            model_name='cliente',
            name='tipo',
            field=models.ForeignKey(to='portalweb.Tipocliente'),
        ),
    ]
