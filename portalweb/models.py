from django.db import models

# Create your models here.
class Tipocliente(models.Model):
    tipo = models.CharField(max_length=30)

    class Meta:
        verbose_name = 'Tipo de cliente'
        verbose_name_plural = 'Tipos de cliente'

    def __str__(self):
        return self.tipo

class Cliente(models.Model):
    tipo = models.ForeignKey(Tipocliente)
    empresa = models.CharField(max_length=30, unique=True, error_messages={'unique':"Ya existe una empresa registrada con este nombre"})
    nit = models.CharField(max_length=30, unique=True, error_messages={'unique':"Ya existe una empresa registrada con este NIT"})
    ciudad = models.CharField(max_length=30)
    contacto = models.CharField(max_length=50)
    telefono = models.CharField(max_length=30)
    email = models.CharField(max_length=50)

    def __str__(self):
        return self.empresa
